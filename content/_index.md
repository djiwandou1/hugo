## Tokdis Technical Document

Tech. Doc ini dibuat menggunakan engine [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) berupa static web page menggunakan format file Markdown (file .md).
Untuk memulai dapat melihat contoh file di `/content/_index.md`. 

Cek Repository: [Tokdis Tech Doc](https://gitlab.com/pages/hugo).
